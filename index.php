<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Tasks</title>
</head>

<body style="display:none">
  <div id="loader"><div id="spinner"></div></div>
  <div class="container mt-3">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 offset-md-3">
            <div class="card" style="width: 18rem;">
              <div class="card-body">
                <h5 class="card-title" id=totalComplete>0</h5>
                <p class="card-text">Completed</p>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="card" style="width: 18rem;">
              <div class="card-body">
                <h5 class="card-title" id=totaloverall>0</h5>
                <p class="card-text">Overall</p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <!-- Button trigger modal -->
        <div class="float-end mb-3">
          <button onclick="openAddTaskModal()" type="button" class="btn btn-primary pull-right" data-bs-toggle="modal" data-bs-target="#TaskModal">
            Add Task
          </button>
        </div>
        <table id="taskList">
          <thead>
            <tr>
              <th>Status</th>
              <th>Priority</th>
              <th>Title</th>
              <th>Description</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>

    </div>
  </div>



  <!-- Modal -->
  <div class="modal fade" id="TaskModal" tabindex="-1" aria-labelledby="TaskModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="TaskModalLabel">Task</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8">Label<input type="text" name="task_label" class="form-control"></div>
            <div class="col-md-4">
              Priority
              <select name="task_priority" id="priority_list" class="form-control">

              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">Description<textarea name="task_description" id="" rows="5" class="form-control"></textarea></div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button type="button" id="addTask" class="btn btn-primary">Add Task</button>
          <button type="button" style="display: none;" id="updateTask" class="btn btn-primary">Update Task</button>
        </div>
      </div>
    </div>
  </div>



  <script src="./app.js"></script>
  <script src="./js/index.js"></script>
</body>

</html>