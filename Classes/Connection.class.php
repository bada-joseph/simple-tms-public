<?Php
namespace TMP\Classes;

use \SQLite3;

class Connection extends SQLite3 {

    const DB = 'database.sqlite3';
    public function __construct()
    {
    }

    public function openConnection()
    {
        if(!file_exists(self::DB)) {
            $this->initialize();
        } else {
            $this->open(self::DB);
        }
        $this->enableExceptions(true);
    }

    public function closeConnection() {
        $this->close();
    }

    private function initialize() {
        $dbFile = @fopen(self::DB,'r+');
        @fclose($dbFile);
        $this->open(self::DB);
        $this->exec('CREATE TABLE tasks (
            id          INTEGER       PRIMARY KEY AUTOINCREMENT,
            label       VARCHAR (255) NOT NULL,
            description TEXT,
            priority    INT,
            status      INT,
            created_at  DATETIME,
            updated_at  DATETIME
        );');
    }
}