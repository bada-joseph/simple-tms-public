<?Php

namespace TMP\Classes\Enumerations;

Class Status {
    const TODO = 0;
    const COMPLETE = 1;
    
    const LABELS = [
        self::TODO      => "To Do",
        self::COMPLETE  => "Complete"
    ];

}