<?Php

namespace TMP\Classes\Enumerations;

Class Action {
    const FETCH_TASKS = 'fetchTasks';
    const CREATE_TASK = 'createTask';
    const UPDATE_TASK = 'updateTask';
    const COMPLETE_TASK = 'completeTask';
    const DELETE_TASK = 'deleteTask';
    const FETCH_PRIORITY_LIST = 'fetchPriorityList';
    const GET_TOTAL_STAT = 'getStat';
}