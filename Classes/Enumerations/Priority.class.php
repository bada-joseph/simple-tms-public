<?Php

namespace TMP\Classes\Enumerations;

class Priority {

    const BLOCKER = 0;
    const CRITICAL = 1;
    const HIGH = 2;
    const MEDIUM = 3;
    const LOW = 4;
    const FOR_IMPROVEMENT = 5;

    const LABELS = [
        self::BLOCKER           => "Blocker",
        self::CRITICAL          => "Critical",
        self::HIGH              => "High Priority",
        self::MEDIUM            => "Medium Priority",
        self::LOW               => "Low Priority",
        self::FOR_IMPROVEMENT   => "For Improvement"
    ];

}