<?php

namespace TMP\Classes;

use TMP\Classes\Enumerations\Priority;
use TMP\Classes\Enumerations\Status;
use \Exception;
use TMP\Classes\Connection;

class TaskRecord
{
    private $conn;
    private $id;
    private $label;
    private $priority;
    private $status = Status::TODO;
    private $description;

    public function __construct($id = null)
    {
        $this->conn = new Connection();
        if (!is_null($id)) {
            $this->load($id);
        }
    }

    public static function totalRecords()
    {
        $conn = new Connection();
        $conn->openConnection();
        $totalRecords = $conn->querySingle("SELECT COUNT(*) FROM tasks");
        $totalComplete = $conn->querySingle("SELECT COUNT(*) FROM tasks WHERE status=" . Status::COMPLETE);
        return ['total' => $totalRecords, 'complete' => $totalComplete];
    }

    public static function fetch()
    {
        $conn = new Connection();
        $conn->openConnection();
        $result = $conn->query("SELECT * FROM tasks");
        $tasks = array();
        while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $row['priority'] = [
                'id' => $row['priority'],
                'label' => Priority::LABELS[$row['priority']]
            ];
            $row['status'] = [
                'id' => $row['status'],
                'label' => Status::LABELS[$row['status']]
            ];
            $tasks[] = $row;
        }

        return $tasks;
    }

    public function save()
    {
        is_null($this->id) ? $this->insert() : $this->update();
    }

    private function insert()
    {
        try {
            $this->conn->openConnection();
            $result = $this->conn->exec("INSERT INTO tasks (label, description, priority, status, created_at, updated_at) 
                VALUES (
                '$this->label', 
                '$this->description',
                $this->priority,
                $this->status,
                CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);");
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
        $this->conn->closeConnection();
    }

    private function update()
    {
        try {
            $this->conn->openConnection();
            $this->conn->exec("UPDATE tasks SET label='$this->label', description='$this->description', priority='$this->priority', status='$this->status', updated_at=CURRENT_TIMESTAMP WHERE id=$this->id");
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
        $this->conn->closeConnection();
    }

    public function markAsComplete()
    {
        try {
            $this->conn->openConnection();
            $this->conn->exec("UPDATE tasks SET status='" . Status::COMPLETE . "', updated_at=CURRENT_TIMESTAMP WHERE id=$this->id");
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
        $this->conn->closeConnection();
    }

    public function delete()
    {
        try {
            $this->conn->openConnection();
            $this->conn->exec("DELETE FROM tasks WHERE id=$this->id");
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
        $this->conn->closeConnection();
    }

    private function load($id)
    {
        $this->id = $id;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of label
     *
     * @return  self
     */
    public function setLabel($label)
    {
        $this->label = $this->conn->escapeString($label);

        return $this;
    }

    /**
     * Get the value of priority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set the value of priority
     *
     * @return  self
     */
    public function setPriority($priority)
    {
        $this->priority = $this->conn->escapeString($priority);

        return $this;
    }

    /**
     * Get the value of description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the value of description
     *
     * @return  self
     */
    public function setDescription($description)
    {
        $this->description = $this->conn->escapeString($description);

        return $this;
    }

    /**
     * Get the value of status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */
    public function setStatus($status)
    {
        $this->status = $this->conn->escapeString($status);

        return $this;
    }
}
