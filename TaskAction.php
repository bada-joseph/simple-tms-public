<?Php

namespace TMP;

require_once('./autoloader.php');

use Exception;
use \TMP\Classes\Enumerations\Action;
use \TMP\Classes\Enumerations\Priority;
use \TMP\Classes\TaskRecord;

class TaskAction
{

    private $action;
    private $response;

    public function __construct($action)
    {
        $this->action = $action;
        return $this->action();
    }

    private function action()
    {

        switch ($this->action) {
            case Action::FETCH_PRIORITY_LIST:
                $this->fetchPriorityList();
                break;
            case Action::CREATE_TASK:
                $this->createTask();
                break;
            case Action::FETCH_TASKS:
                $this->fetchTask();
                break;
            case Action::UPDATE_TASK:
                $this->updateTask();
                break;
            case Action::COMPLETE_TASK:
                $this->completeTask();
                break;
            case Action::DELETE_TASK:
                $this->deleteTask();
                break;
            case Action::GET_TOTAL_STAT:
                $this->getStatistics();
                break;
            default:
                throw new Exception("Not Accepted");
        }
    }

    private function getStatistics()
    {
        $this->response = TaskRecord::totalRecords();
    }

    private function deleteTask()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception("Incorrect Method");
        }
        $taskRecord = new TaskRecord($_POST['id']);
        $taskRecord->delete();
    }

    private function completeTask()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception("Incorrect Method");
        }
        $taskRecord = new TaskRecord($_POST['id']);
        $taskRecord->markAsComplete();
    }

    private function fetchPriorityList()
    {
        $this->response = Priority::LABELS;
    }

    private function updateTask()
    {

        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception("Incorrect Method");
        }
        $taskRecord = new TaskRecord();
        $taskRecord->setLabel($_POST['label'])
            ->setDescription($_POST['description'])
            ->setPriority($_POST['priority'])
            ->setId($_POST['id'])
            ->save();
    }

    private function createTask()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            throw new Exception("Incorrect Method");
        }
        $taskRecord = new TaskRecord();
        $taskRecord->setLabel($_POST['label'])
            ->setDescription($_POST['description'])
            ->setPriority($_POST['priority'])
            ->save();
    }

    private function fetchTask()
    {
        $this->response = TaskRecord::fetch();
    }

    /**
     * Get the value of response
     */
    public function getResponse()
    {
        return json_encode($this->response);
    }
}



$action = new TaskAction((isset($_REQUEST['action'])) ? $_REQUEST['action'] : "fetchTasks");

echo json_encode($action->getResponse());
