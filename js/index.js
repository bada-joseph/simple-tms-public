$(function () {

    $('#addTask').on('click', function () {
        $('#TaskModal button.btn-close').trigger("click");
        load.spinner.show();
        $.post('./TaskAction.php', {
            'action': 'createTask',
            'label': $('input[name = "task_label"]').val(),
            'description': $('textarea[name = "task_description"]').val(),
            'priority': $('select[name = "task_priority"]').val(),
        }, function () {
            load.spinner.hide();
            $('[name^= "task_"]').val("");
            alert("Task Successfully Added");
            load.tasks();
        })
    });

    $('#updateTask').on('click', function () {
        $('#TaskModal button.btn-close').trigger("click");
        load.spinner.show();
        $.post('./TaskAction.php', {
            'action': 'updateTask',
            'id': $('#updateTask').data().id,
            'label': $('input[name = "task_label"]').val(),
            'description': $('textarea[name = "task_description"]').val(),
            'priority': $('select[name = "task_priority"]').val(),
        }, function () {
            load.spinner.hide();
            $('[name^= "task_"]').val("");
            alert("Task Successfully Updated");
            load.tasks();
        });
    });
    load.init();
    $('body').show();
})

var load = {
    init: function () {
        this.spinner.init();
        this.priorityList();
        this.tasks();
        this.stats();
    },
    spinner: {
        init: function () {
            this.hide();
        },
        show: function () {
            $('#loader').show();
        },
        hide: function () {
            $('#loader').hide();
        }
    },
    priorityList: function () {
        this.spinner.show();
        $.get('./TaskAction.php', {
            'action': 'fetchPriorityList'
        }, function (response) {
            load.spinner.hide();
            $.each(JSON.parse(response), function (i, item) {
                $('#priority_list').append($('<option>', {
                    'value': i,
                    'text': item
                }));
            });
            $('#priority_list').val("");
        }, 'json');
    },
    tasks: function () {
        this.spinner.show();
        $('#taskList').hide()
        $.get('./TaskAction.php', {
            'action': 'fetchTasks'
        }, function (response) {
            if ($.fn.dataTable.isDataTable('#taskList')) {
                $('#taskList').DataTable().destroy();
            }
            $('#taskList tbody').text("");
            load.spinner.hide();
            var tasks = JSON.parse(response)

            $.each(tasks, function (i, item) {
                $('#taskList tbody').append(`
                <tr>
                    <td class="status" data-order="` + item.status.id + `">` + item.status.label + `</td>
                    <td class="priority" data-order="` + item.priority.id + `">` + item.priority.label + `</td>
                    <td class="label">` + item.label + `</td>
                    <td class="description"">` + item.description + `</td>
                    <td>  
                        <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        Actions
                        </button>
                        <ul class="dropdown-menu">
                        <li><a onClick="taskAction(this)" class="dropdown-item" href="#" data-value="`+ item.id + `" data-action="editTask">Edit</a></li>
                        <li><a onClick="taskAction(this)" class="dropdown-item" href="#" data-value="`+ item.id + `" data-action="deleteTask">Delete</a></li>
                        `+ ((item.status.label === "Complete") ?
                        "" : `<li><a onClick="taskAction(this)" class="dropdown-item" href="#" data-value="` + item.id + `" data-action="completeTask">Complete</a></li>`) +
                    `</ul>
                        </div>
                  </td>    
                </tr>
                `);
            });
            $('#taskList').show();
            $('#taskList').DataTable();
            load.stats();
        }, 'json')
    },
    stats: function () {
        $.get('./TaskAction.php', {
            'action': 'getStat'
        }, function (response) {
            var stat = JSON.parse(response);
            console.log(stat);
            $('#totalComplete').text(stat.complete);
            $('#totaloverall').text(stat.total);
        }, 'json');
    }
}

var TaskModal = {
    add: function () {
        $('#TaskModalLabel').text("New Task");
        $('#addTask').show();
        $('#updateTask').data().id = null;
        $('#updateTask').hide();
    },
    edit: function (id, label, description, priority) {
        $('#TaskModalLabel').text("Update Task");
        $('#addTask').hide();
        $('#updateTask').show();
        $('#updateTask').data().id = id;
        $('input[name=task_label]').val(label);
        $('textarea[name=task_description]').val(description);
        $('select[name=task_priority]').val($('select[name=task_priority]').find('option:contains("' + priority + '")').val());
    }
}

function taskAction(button) {
    var value = $(button).data().value;
    var action = $(button).data().action;
    var $row = $($(button).parents()[4]);
    switch (action) {
        case "editTask":
            var modal = new bootstrap.Modal(document.getElementById("TaskModal"), {});
            TaskModal.edit(value, $row.find('.label').text(), $row.find('.description').text(), $row.find('.priority').text());
            modal.show();
            break;
        case "completeTask":
            load.spinner.show();
            $.post('./TaskAction.php', {
                'action': 'completeTask',
                'id': value
            }, function () {
                load.spinner.hide();
                load.tasks();
                alert("Task marked as completed");
            });
            break;
        case "deleteTask":
            var isDelete = confirm("Are you sure you want to delete?");
            if (isDelete) {
                $.post('./TaskAction.php', {
                    'action': 'deleteTask',
                    'id': value
                }, function () {
                    load.spinner.hide();
                    load.tasks();
                    alert("Task is deleted!");
                });
            }

            break;

    }
    console.log(value, action);
}

function openAddTaskModal() {
    TaskModal.add();
    return true;
}
