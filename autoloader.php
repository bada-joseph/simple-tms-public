<?Php
spl_autoload_register(function ($class_name) {
    $file = explode('TMP\\', $class_name)[1] . '.class.php';
    require $file;
});