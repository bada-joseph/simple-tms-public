const path = require('path');

module.exports = {
  entry: './init.js',
  mode: 'development',
  output: {
    filename: 'app.js',
    path: path.resolve(__dirname, './'),
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.(sass|css|scss)$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'public/images/[name].[hash].[ext]',
            publicPath: '../../'
          }
        }
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'public/fonts/[name].[hash].[ext]',
            publicPath: '../../'
          }
        }
      }
    ]
  }
};